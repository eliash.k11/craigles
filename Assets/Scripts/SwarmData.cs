using UnityEngine;

public class SwarmData
{
    public Vector3 Center;
    public Vector3 Direction;
    public Vector3 AvoidDirection;
    public float DistanceToClosestCraigle;
    public int AvoidCount;
    public float Count;
    public Vector3 OwnerPosition;


    public void Clear()
    {
        Center = Vector3.zero;
        Direction = Vector3.zero;
        AvoidDirection = Vector3.zero;
        DistanceToClosestCraigle = int.MaxValue;
        AvoidCount = 0;
        Count = 0;
    }

    public void AddCraigle(CraigleModel craigle, float distance)
    {
        Count++;
        Center += craigle.Position;
        Direction += craigle.Forward;

        if (distance <= craigle.Size * 2)
        {
            AvoidCount++;
            AvoidDirection += OwnerPosition - craigle.Position;
            if (distance < DistanceToClosestCraigle)
                DistanceToClosestCraigle = distance;
        }
    }

    public void ComputeAverages()
    {
        Center /= Count;
        Direction /= Count;
        if (AvoidCount > 0)
            AvoidDirection /= AvoidCount;
    }
}