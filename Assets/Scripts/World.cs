using TMPro;
using UnityEngine;

public class World : MonoBehaviour
{
    [SerializeField] private Craigle _craiglePrefab;
    [SerializeField] private CraigleSpawner _spawner;
    [SerializeField] private Transform _box;
    [SerializeField] private TMP_Text _entitiesActiveText;

    private CraiglePool _craiglePool;
    private SpatialPartitioning _spatialPartitioning;
    private CraigleController _craigleController;

    private void Awake()
    {
        _spatialPartitioning = new SpatialPartitioning(_box, 100, 10);
        _craiglePool = new CraiglePool(_craiglePrefab, 1000);
        _craigleController = new CraigleController(_craiglePool, _spawner, _spatialPartitioning);
    }

    private void Start()
    {
        _spawner.transform.position = _spatialPartitioning.GetCornerPosition(SpatialPartitioning.Corner.TopRight);
        _spawner.transform.LookAt(_box.transform.position);
        _spawner.transform.Translate(Vector3.forward * 5);
    }

    private void Update()
    {
        _entitiesActiveText.text = $"Entities Active: {_craiglePool.ActiveCraigles.Count}";
    }
    private void LateUpdate()
    {
        _craigleController.LateUpdate();
    }
}
