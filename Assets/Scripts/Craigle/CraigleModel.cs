using UnityEngine;

public class CraigleModel
{
    public int Id;
    public Vector3 HeadPosition;
    public Vector3 Position;
    public Vector3 OldPosition;
    public Vector3 Forward;
    public float Size;
    public bool Spawned;
    public Color Color;
    private Mitosis _mitosis = new();
    public SwarmData SwarmData = new();
    public CraigleModel NextCraigle;
    public CraigleModel PreviousCraigle;

    public bool CanSplit => _mitosis.CanSplit;

    public void UpdateData(Craigle craigle)
    {
        HeadPosition = craigle.HeadPosition;
        Position = craigle.Position;
        Forward = craigle.Forward;
        Size = craigle.Scale.x;

        if (Spawned)
            Reset();

        SwarmData.Clear();
        SwarmData.OwnerPosition = Position;
        _mitosis.Grow();
    }

    public void Reset()
    {
        OldPosition = Position;
        Spawned = false;
        _mitosis.Reset();
    }
}