using UnityEngine;

public class Craigle : MonoBehaviour
{
    #region Data
    public int Id { get; private set; }
    public Vector3 Position => transform.position;
    public Vector3 Forward => transform.forward;
    public Vector3 HeadPosition => _head.transform.position;
    public Vector3 Scale { get; private set; }
    public Color Color { get; private set; }
    public float Velocity { get; private set; }

    #endregion

    private float _baseSpeed = 10f;
    private float _currentSpeed = 0f;

    [SerializeField] private SpriteRenderer _visuals;
    [SerializeField] private Transform _head;


    #region Public Methods
    public void SetId(int id) => Id = id;

    public void SetVelocityWithDirection(float velocity, Vector3 direction)
    {
        Velocity = velocity;
        transform.forward = direction;
    }
    public void SetVelocity(float velocity) => Velocity = velocity;
    public void SetSize(Vector3 size) => transform.localScale = Scale = size;
    public void SetColor(Color color) => _visuals.color = Color = color;

    #endregion


    #region Unity Callbacks
    private void OnDisable()
    {
        transform.localScale = Vector3.one;
    }
    #endregion


    public void Move()
    {
        Velocity = Mathf.Lerp(Velocity, _baseSpeed, Time.deltaTime * Time.deltaTime);
        _currentSpeed = Mathf.Lerp(_baseSpeed, Velocity, Time.deltaTime * Time.deltaTime);
        transform.Translate(Vector3.forward * _currentSpeed * Scale.x * Time.deltaTime);
    }
}