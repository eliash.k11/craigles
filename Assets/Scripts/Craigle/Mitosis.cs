using UnityEngine;

public class Mitosis
{
    private float _growth = 0;
    private bool _isReady => _growth == 1;
    public void Reset() => _growth = 0;
    public void Grow() => _growth = Mathf.Min(_growth + .15f * Time.deltaTime, 1);
    public bool CanSplit => _isReady && UnityEngine.Random.Range(0, 100) <= 20;
    public static void Split(Vector3 position, Vector3 scale, Vector3 forward, Vector3 up, Color color, float velocity, CraigleController.GetCraigleToSpawnDelegate getCraigle)
    {
        var craigleA = getCraigle();
        var craigleB = getCraigle();
        craigleA.transform.position = craigleB.transform.position = position;

        craigleA.SetVelocityWithDirection(velocity, (Quaternion.AngleAxis(30, up) * forward).normalized);
        craigleB.SetVelocityWithDirection(velocity, (Quaternion.AngleAxis(-30, up) * forward).normalized);

        Color shiftHue(Color color, float shift)
        {
            Color.RGBToHSV(color, out var h, out var s, out var v);

            h += shift;

            if (h > 1)
                h -= 1;
            else if (h < 0)
                h += 1;

            return Color.HSVToRGB(h, s, v);
        }

        craigleA.SetSize(scale * 0.7f);
        craigleA.SetColor(shiftHue(color, 0.0277f));
        craigleB.SetSize(scale * 0.7f);
        craigleB.SetColor(shiftHue(color, -0.0277f));
    }
}