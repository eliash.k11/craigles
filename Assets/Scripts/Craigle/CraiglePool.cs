using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CraiglePool
{
    private Transform _poolParent;
    private Craigle _craiglePrefab;

    private List<Craigle> _activeCraigles = new List<Craigle>();
    private List<Craigle> _inactiveCraigles = new List<Craigle>();

    public IReadOnlyList<Craigle> ActiveCraigles => _activeCraigles;

    public CraiglePool(Craigle prefab, int size)
    {
        _poolParent = new GameObject("Craigles").transform;
        _craiglePrefab = prefab;

        for (var i = 0; i < size; ++i)
            _inactiveCraigles.Add(CreateCraigle(i));
    }

    public IEnumerable<Craigle> GetAllInstances() => _activeCraigles.Concat(_inactiveCraigles);

    public bool TryGetInactiveCraigle(out Craigle craigle)
    {
        if (_inactiveCraigles.Count == 0)
        {
            craigle = GetRandomActiveCraigle();
            return false;
        }

        craigle = _inactiveCraigles[0];
        _inactiveCraigles.RemoveAt(0);
        _activeCraigles.Add(craigle);

        craigle.gameObject.SetActive(true);

        return true;
    }

    public void ReturnCraigle(Craigle craigle)
    {
        craigle.gameObject.SetActive(false);

        for (var i = 0; i < _activeCraigles.Count; ++i)
        {
            var active = _activeCraigles[i];
            if (active.Id != craigle.Id)
                continue;

            _activeCraigles.RemoveAt(i);
            break;
        }

        _inactiveCraigles.Add(craigle);
    }

    private Craigle GetRandomActiveCraigle() => _activeCraigles[Random.Range(0, _activeCraigles.Count)];

    private Craigle CreateCraigle(int id)
    {
        var craigle = Object.Instantiate(_craiglePrefab, _poolParent);
        craigle.SetId(id);
        craigle.gameObject.SetActive(false);
        return craigle;
    }
}