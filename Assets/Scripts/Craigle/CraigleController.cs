using System;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

public class CraigleController
{
    public delegate Craigle GetCraigleToSpawnDelegate();

    private CraiglePool _craiglePool;
    private CraigleSpawner _craigleSpawner;
    private SpatialPartitioning _spatialPartitioning;
    private Craigle[] _activeCraigles = Array.Empty<Craigle>();

    private CraigleModel[] _craigleModels = new CraigleModel[1000];

    public CraigleController(CraiglePool pool, CraigleSpawner spawner, SpatialPartitioning spatialPartitioning)
    {
        _craiglePool = pool;
        _craigleSpawner = spawner;
        _spatialPartitioning = spatialPartitioning;

        _craigleSpawner.SetGetCraigleDelegate(GetCraigleToSpawn);

        for (int i = 0; i < _craigleModels.Length; i++)
            _craigleModels[i] = new CraigleModel() { Id = i };
    }


    public void LateUpdate()
    {
        _craigleSpawner.UpdateSpawner();

        _activeCraigles = _craiglePool.ActiveCraigles.ToArray();
        for (var i = 0; i < _activeCraigles.Length; ++i)
        {
            var craigleEntity = _activeCraigles[i];
            var craigleModel = _craigleModels[craigleEntity.Id];

            if (craigleModel.SwarmData.Count >= 100 && UnityEngine.Random.Range(0, 100) <= 20)
            {
                Debug.Log("Despawn");
                DespawnCraigle(craigleEntity, craigleModel);
                continue;
            }
            else if (craigleModel.CanSplit)
            {
                SplitCraigles(craigleEntity, craigleModel);
                continue;
            }

            craigleModel.UpdateData(craigleEntity);

            if (_spatialPartitioning.IsMovingOutOfBounds(craigleModel, out var wallNormalVector))
                craigleEntity.transform.forward = Vector3.Reflect(craigleModel.Forward, wallNormalVector);
            else
                ApplyRules(craigleEntity, craigleModel);

            craigleEntity.Move();
            _spatialPartitioning.CraigleMoved(craigleModel);

            craigleModel.OldPosition = craigleModel.Position;
        }
    }

    private void ApplyRules(Craigle craigleEntity, CraigleModel craigleModel)
    {
        _spatialPartitioning.FindCraiglesWithinSwarmDistance(craigleModel, 10);

        if (craigleModel.SwarmData.Count <= 0)
        {
            return;
        }

        craigleModel.SwarmData.ComputeAverages();

        var swarmData = craigleModel.SwarmData;

        var distanceToCenter = (swarmData.Center - craigleModel.Position).magnitude;
        Vector3 direction = (swarmData.Center + swarmData.AvoidDirection - craigleModel.Position).normalized;

        if (direction != Vector3.zero)
        {
            var rotSpeedDistance = Mathf.Min(200, Mathf.Max(0, math.remap(0, 10, 200, 0, distanceToCenter)));
            var rotSpeedAngleDiff = Mathf.Min(200, Mathf.Max(0, math.remap(0, 180, 0, 200, Vector3.Angle(craigleModel.Forward, direction))));

            var rotSpeed = Mathf.Min(200, rotSpeedDistance + rotSpeedAngleDiff);
            craigleEntity.transform.forward = Vector3.RotateTowards(craigleModel.Forward, direction, Mathf.Deg2Rad * rotSpeed * Time.deltaTime, 0);
        }

        float speedToCenter = Math.Min(Mathf.Max(0, math.remap(0, 10, 40, 0, distanceToCenter)), 40);
        float avoidSpeed = swarmData.AvoidCount == 0 ? 0 : Mathf.Min(Mathf.Max(0, math.remap(0, craigleModel.Size * 2, 100, 0, swarmData.DistanceToClosestCraigle)));

        craigleEntity.SetVelocity(Math.Max(speedToCenter, avoidSpeed));

    }

    #region Spawn & Despawn Craigles
    private void DespawnCraigle(Craigle craigleEntity, CraigleModel craigleModel)
    {
        _craiglePool.ReturnCraigle(craigleEntity);
        _spatialPartitioning.Remove(craigleModel);
        craigleModel.PreviousCraigle = null;
        craigleModel.NextCraigle = null;
    }

    private void SplitCraigles(Craigle craigleEntity, CraigleModel craigleModel)
    {
        DespawnCraigle(craigleEntity, craigleModel);
        Mitosis.Split(craigleModel.Position, craigleEntity.Scale, craigleEntity.transform.up, craigleModel.Forward, craigleEntity.Color, craigleEntity.Velocity, GetCraigleToSpawn);
    }

    private Craigle GetCraigleToSpawn()
    {
        if (!_craiglePool.TryGetInactiveCraigle(out var craigle))
            DespawnCraigle(craigle, _craigleModels[craigle.Id]);

        _craigleModels[craigle.Id].Spawned = true;
        return craigle;
    }
    #endregion
}