using UnityEngine;

public class CraigleSpawner : MonoBehaviour
{
    private CraigleController.GetCraigleToSpawnDelegate _getCraigle;
    private float _spawnTimer = 1;

    public void SetGetCraigleDelegate(CraigleController.GetCraigleToSpawnDelegate getCraigle) => _getCraigle = getCraigle;

    public void UpdateSpawner()
    {
        _spawnTimer += Time.deltaTime;
        if (_spawnTimer >= 1)
        {
            SpawnCraigle();
            _spawnTimer = 0;
        }
    }

    private void SpawnCraigle()
    {
        var craigle = _getCraigle();
        var direction = (Random.insideUnitSphere + transform.forward).normalized;

        craigle.transform.position = transform.position;

        craigle.SetVelocityWithDirection(Random.Range(30, 50), direction);
        craigle.SetSize(Vector3.one);
        craigle.SetColor(Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f));
    }
}
