using UnityEngine;

public class SpatialPartitioning
{
    private Vector3 _boxPosition;
    private int _cellSize;
    private int _boxSize;
    private int _cellsInRow;

    Cell[,,] _cells;

    public SpatialPartitioning(Transform box, int boxSize, int cellSize)
    {
        _boxSize = boxSize;
        _cellSize = cellSize;
        _cellsInRow = _boxSize / _cellSize;
        _cells = new Cell[_cellsInRow, _cellsInRow, _cellsInRow];

        _boxPosition = box.transform.position;

        var startPosition = new Vector3(_boxPosition.x - (_cellsInRow / 2 * _cellSize) + _cellSize / 2, _boxPosition.y - (_cellsInRow / 2 * _cellSize) + _cellSize / 2, _boxPosition.z - (_cellsInRow / 2 * _cellSize) + _cellSize / 2);

        for (int x = 0; x < _cellsInRow; x++)
        {
            for (int y = 0; y < _cellsInRow; y++)
            {
                for (int z = 0; z < _cellsInRow; z++)
                {
                    Vector3 cellPosition = new Vector3(startPosition.x + _cellSize * x, startPosition.y + _cellSize * y, startPosition.z + _cellSize * z);
                    _cells[x, y, z] = new Cell() { Center = cellPosition, Size = _cellSize, Color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f) };
                }
            }
        }

        //debug
        // foreach (var cell in cells)
        // {
        //     var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //     sphere.transform.position = cell.Center;
        //     sphere.transform.localScale = Vector3.one * _cellSize;
        // }
    }

    public void CraigleMoved(CraigleModel craigle)
    {
        var currentCellIndices = GetCellIndices(craigle.Position);
        var oldCellIndices = GetCellIndices(craigle.OldPosition);

        //check if craigle has moved to another cell
        if (currentCellIndices.Item1 == oldCellIndices.Item1 && currentCellIndices.Item2 == oldCellIndices.Item2 && currentCellIndices.Item3 == oldCellIndices.Item3)
            return;

        //unlink from previous cell
        Remove(craigle, oldCellIndices);

        //link to a new cell
        Add(craigle, currentCellIndices);
    }

    public void Add(CraigleModel craigle, (int x, int y, int z)? cellIndices = null)
    {
        cellIndices = cellIndices.HasValue ? cellIndices : GetCellIndices(craigle.Position);

        //link to a cell
        var cell = _cells[cellIndices.Value.x, cellIndices.Value.y, cellIndices.Value.z];
        craigle.Color = cell.Color;

        craigle.PreviousCraigle = null;
        craigle.NextCraigle = cell.First;

        cell.First = craigle;

        if (craigle.NextCraigle != null)
        {
            craigle.NextCraigle.PreviousCraigle = craigle;
        }
    }

    public void Remove(CraigleModel craigle, (int x, int y, int z)? cellIndices = null)
    {
        cellIndices = cellIndices.HasValue ? cellIndices : GetCellIndices(craigle.Position);

        if (craigle.PreviousCraigle != null)
            craigle.PreviousCraigle.NextCraigle = craigle.NextCraigle;

        if (craigle.NextCraigle != null)
            craigle.NextCraigle.PreviousCraigle = craigle.PreviousCraigle;

        var oldCell = _cells[cellIndices.Value.x, cellIndices.Value.y, cellIndices.Value.z];
        if (oldCell.First == craigle)
            oldCell.First = craigle.NextCraigle;
    }

    public void FindCraiglesWithinSwarmDistance(CraigleModel craigle, int distance)
    {
        var cellIndices = GetCellIndices(craigle.Position);

        var craigleCell = _cells[cellIndices.x, cellIndices.y, cellIndices.z];
        FindCraiglesWithinSwarmDistanceForCell(craigleCell, craigle, distance);

        if (cellIndices.x > 0)
            FindCraiglesWithinSwarmDistanceForCell(_cells[cellIndices.x - 1, cellIndices.y, cellIndices.z], craigle, distance);
        if (cellIndices.x < _cellsInRow - 1)
            FindCraiglesWithinSwarmDistanceForCell(_cells[cellIndices.x + 1, cellIndices.y, cellIndices.z], craigle, distance);
        if (cellIndices.y > 0)
            FindCraiglesWithinSwarmDistanceForCell(_cells[cellIndices.y - 1, cellIndices.y, cellIndices.z], craigle, distance);
        if (cellIndices.y < _cellsInRow - 1)
            FindCraiglesWithinSwarmDistanceForCell(_cells[cellIndices.y + 1, cellIndices.y, cellIndices.z], craigle, distance);
        if (cellIndices.z > 0)
            FindCraiglesWithinSwarmDistanceForCell(_cells[cellIndices.z - 1, cellIndices.y, cellIndices.z], craigle, distance);
        if (cellIndices.z < _cellsInRow - 1)
            FindCraiglesWithinSwarmDistanceForCell(_cells[cellIndices.z + 1, cellIndices.y, cellIndices.z], craigle, distance);
    }

    private void FindCraiglesWithinSwarmDistanceForCell(Cell cell, CraigleModel craigle, int swarmDistanceThreshold)
    {
        var tempCraigle = cell.First;

        while (tempCraigle != null)
        {
            if (tempCraigle.Id != craigle.Id)
            {
                var distanceToCraigle = Vector3.Distance(craigle.Position, tempCraigle.Position);
                if (distanceToCraigle <= swarmDistanceThreshold)
                    craigle.SwarmData.AddCraigle(tempCraigle, distanceToCraigle);
            }

            tempCraigle = tempCraigle.NextCraigle;
        }
    }

    public Vector3 GetCornerPosition(Corner corner)
    {
        return corner switch
        {
            Corner.BottomLeft => _cells[0, 0, 0].Center + new Vector3(-_cellSize / 2, -_cellSize / 2, -_cellSize / 2),
            Corner.BottomRight => _cells[0, 0, 0].Center + new Vector3(_cellSize / 2, _cellSize / 2, _cellSize / 2),
            Corner.TopLeft => _cells[0, _cellsInRow - 1, _cellsInRow - 1].Center + new Vector3(-_cellSize / 2, -_cellSize / 2, -_cellSize / 2),
            Corner.TopRight => _cells[_cellsInRow - 1, _cellsInRow - 1, _cellsInRow - 1].Center + new Vector3(_cellSize / 2, _cellSize / 2, _cellSize / 2),
            _ => Vector3.zero
        };
    }

    public bool IsMovingOutOfBounds(CraigleModel craigle, out Vector3 reflectVector)
    {
        var futurePosition = craigle.Position + craigle.Forward * 2;
        var isOutX = futurePosition.x <= _boxPosition.x - _boxSize / 2f || futurePosition.x >= _boxPosition.x + _boxSize / 2f;
        var isOutY = futurePosition.y <= _boxPosition.y - _boxSize / 2f || futurePosition.y >= _boxPosition.y + _boxSize / 2f;
        var isOutZ = futurePosition.z <= _boxPosition.z - _boxSize / 2f || futurePosition.z >= _boxPosition.z + _boxSize / 2f;

        reflectVector = Vector3.zero;

        if (isOutX)
        {
            reflectVector = futurePosition.x < _boxPosition.x ? Vector3.right : Vector3.left;
            return true;
        }
        if (isOutY)
        {
            reflectVector = futurePosition.y < _boxPosition.y ? Vector3.up : Vector3.down;
            return true;
        }
        if (isOutZ)
        {
            reflectVector = futurePosition.z < _boxPosition.z ? Vector3.forward : Vector3.back;
            return true;
        }

        return false;
    }

    private (int x, int y, int z) GetCellIndices(Vector3 position) => ((int)(position.x / _cellSize), (int)(position.y / _cellSize), (int)(position.z / _cellSize));

    private class Cell
    {
        public Vector3 Center;
        public float Size;
        public Color Color;
        public CraigleModel First;
    }

    public enum Corner
    {
        BottomLeft,
        BottomRight,
        TopLeft,
        TopRight
    }
}
