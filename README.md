I'd like to shortly sum up my work. 
I genuenly think it was a great practice as I haven't got much physics experience and to be honset, this project beat the crap out of me. 

I approached the simulation from 3 difference angles:

1) simulation based on manual Quaternion/velocity computations
    In the beginning it was fine, but it collapsed as soon as I started implementing swarming behvaiour. Craigles moving outside of the box due to transform.Translate forced it way through the wall due to the continous apply of swarm's direction. I ditched the idea when I though I had to use Raycasts to fix the collision detection.

2) pure physics
    Smoother and well more optimised than I expected. I tried to continously apply Force and Torque, both based on the swarm state to navigate the Craigle. Unfortunately I didn't know how to calculate torque combined with angular drag to achieve at least some kind of swarm simulation.

3) mix of rigidbody based force with manual rotation computation
    Handling the velocity and collisions on Physics level solved issues I faced in case 1).
    Force is applied either towards a swarm or away from the colliding craigle. 
    Craigle rotates to the Velocity vector insted of applying torque. 
    It's not perfect, but at least I had the idea what I'm doing in the calculations. 

    Craigle behaviour is state based.
    The good side, the behaviour is kind of fixed, easier to predict.
    The bad side, it is possibly too strict to provide smooth simulation, especially when switching between AlignWithSwarmState & MoveAwayState.

Issues:
1) Because of manual rotation calculation, the interpolated rotation step is somethimes too large. I guess it must be the too-radical change of velocity in the opposite direction
2) My PC handled 700 entities before melting down

Possible further optimization:
1) Store position and forward vectors on each Craigle update loop to reduce Transform.position/Transform.forward accessors within SwarmEntityData
2) Convert to ECS, of course...

I'd appreciate any tips on how to finetune the simulation. Thank you. 

